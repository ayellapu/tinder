
/**
 * This is a class to implement to store the all users in the user collections
 * @author ayellapu1
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class UserCollection {

	public ArrayList<User> userlist;

	// create constructor to declare the array list
	public UserCollection() {
		this.userlist = new ArrayList<User>();
	}

	/**
	 * implement the createNewUser method create , validate the user and store the
	 * user details
	 * 
	 * @param name           name of the user
	 * @param gender_type
	 * @param marital_status
	 * @param location 
	 * @param phone          phone number for the user
	 * @param status         status of the user
	 * @return true if new user created
	 * @return false if user not created
	 */
	public boolean createNewUser(String name, int age, String designation, float weight, String food, String[] foods,
			String hobby, String[] hobbies, Gender gender_type, MaritalStatus marital_status,Qualification[]  qualifications,String location) {
		User newuser = new User(name, age, designation, weight, food, foods, hobby, hobbies, gender_type,
				marital_status,qualifications,location);
		this.userlist.add(newuser);
		return true;
	}

	/**
	 * This is a helper method to print all the users from array list
	 */
	public void returnAllUsers() {
		System.out.println(this.userlist.size());
		for (Iterator<User> repeat = this.userlist.iterator(); repeat.hasNext();) {
			User currentuser = repeat.next();
			System.out.println(currentuser);
		}
	}

	/**
	 * this is a helper array list of type user for finding the matched user
	 * 
	 * @param SearchString
	 * @return
	 */
	public LinkedHashSet<User> getMatch(String Searchname) {
		LinkedHashSet<User> returnMyResults = new LinkedHashSet<User>();
		for (User Currentuser : userlist) {
			if (Currentuser.name == Searchname) {
				returnMyResults.add(Currentuser);
			}
		}
		return returnMyResults;
	}

	/**
	 * This was a helper Array list of type user method for finding the matched user
	 * hobbies of another user
	 * 
	 * @param hobby
	 * @return
	 */
	public ArrayList<User> getNameMatch(String username) {
		ArrayList<User> selectedset = new ArrayList<User>();
		System.out.println("-----------------");
		System.out.println("***SELECTED SET***");
		System.out.println("-----------------");
		User foundUser = null;
		boolean checkuserlist = false;
		for (User currentuser : userlist) {
			if (currentuser.name == username) {
				foundUser = currentuser;
				checkuserlist = true;
			}
		}
		if (!checkuserlist) {
			System.out.println("name doesnot exist");
		}
		String hobby1 = foundUser.hobby;
		System.out.println("The primary hobby of selected user is:" + hobby1);
		for (int j = 0; j < userlist.size(); j++) {
			// Doing the self skip
			User newuser = userlist.get(j);
			if (newuser.name == username) {
				continue;
			}
			for (int k = 0; k < newuser.hobbies.length; k++) {
				if (newuser.hobbies[k].contains(hobby1)) {
					selectedset.add(newuser);
				}
			}
		}
		return selectedset;
	}

	/**
	 * This is a helper method to find the hobbies
	 * 
	 * @param hobby2
	 * @return
	 */
	public ArrayList<User> getHobbyList(String hobby2) {
		ArrayList<User> hobbylist = new ArrayList<User>();
		for (User cuurentuser : userlist) {
			if (cuurentuser.hobby == hobby2) {
				hobbylist.add(cuurentuser);	
			}
		}
		return hobbylist;
	}

	/**
	 * This is a helper method to find users having either or hobbies
	 * 
	 * @param string[] playthings to store the hobbies
	 * @return
	 */
	public ArrayList<User> getHobbyOrList(String[] playingthings) {
		HashSet<User> getHobbyORlist = new HashSet<User>();
		for (int i = 0; i < this.userlist.size(); i++) {
			User newuserfound = this.userlist.get(i);
			for (int j = 0; j < newuserfound.hobbies.length; j++) {
				for (int k = 0; k < playingthings.length; k++) {
					if (newuserfound.hobbies[j] == playingthings[k]) {
						getHobbyORlist.add(newuserfound);
					}
				}
			}
		}
		return new ArrayList<User>(getHobbyORlist);
	}

	/**
	 * This is a helper method to find the users having both the hobbies
	 * 
	 * @param playthings array of hobbies
	 * @return array list having users who are having both the hobbies
	 */
	public ArrayList<User> getHobbyAndList(String[] playthings) {
		HashSet<User> getHobbyANDSet = new HashSet<User>();
		for (int i = 0; i < this.userlist.size(); i++) { // iterating through each user in user list

			User newuser = this.userlist.get(i);
			List<String> newuserhobbies = Arrays.asList(newuser.hobbies);
			boolean areAllFound = true;
			for (String hobbyToSearch : playthings) {
				if (!newuserhobbies.contains(hobbyToSearch)) {
					areAllFound = false;
				}
			}
			if (areAllFound) {
				getHobbyANDSet.add(newuser);
			}
		}

		return new ArrayList<User>(getHobbyANDSet);
	}

	/**
	 * This is a helper method to return the users by designation
	 * 
	 * @param profession profession of the user
	 * @return
	 */
	public ArrayList<User> getDesignation(String profession) {
		ArrayList<User> designationlist = new ArrayList<User>();
		for (User currentuser : this.userlist) {
			if (currentuser.designation == profession) {
				designationlist.add(currentuser);
			}
		}

		return designationlist;
	}

	/**
	 * This is a helper method to return the users by minimum weight
	 * 
	 * @param minweight minimum weight of the user
	 * @return
	 */
	public ArrayList<User> getMinWeight(float minweight) {
		ArrayList<User> minweighlist = new ArrayList<User>();
		User newuser = null;
		for (User currentuser : this.userlist) {
			if (currentuser.weight < minweight) {
				newuser = currentuser;
				minweighlist.add(newuser);
			}
		}
		return minweighlist;
	}

	/**
	 * This is a helper method to return the users by maximum weight
	 * 
	 * @param maxweight maximum weight of the user
	 * @return
	 */
	public ArrayList<User> getMaxWeight(float maxweight) {
		ArrayList<User> maxweighlist = new ArrayList<User>();
		for (int i = 0; i < this.userlist.size(); i++) {
			User newuser = this.userlist.get(i);
			if (newuser.weight > maxweight) {
				maxweighlist.add(newuser);
			}
		}

		return maxweighlist;
	}

	/**
	 * This is a helper method to return the users by in between weights
	 * 
	 * @param minweight minimum weight of the user
	 * @param maxweight maximum weight of the user
	 * @return
	 */

	public ArrayList<User> getBetWeight(float minweight, float maxweight) {
		ArrayList<User> betweightlist = new ArrayList<User>();
		for (int i = 0; i < this.userlist.size(); i++) {
			User newuser = this.userlist.get(i);
			if ((newuser.weight > minweight) && (newuser.weight < maxweight)) {
				betweightlist.add(newuser);
			}
		}
		return betweightlist;
	}

	/**
	 * This is a helper method to return the users by their food
	 * 
	 * @param item food item of the particular user
	 * @return
	 */
	public ArrayList<User> getFoodMatch(String item) {
		ArrayList<User> foodlist = new ArrayList<User>();
		for (User curruser : this.userlist) {
			if (curruser.food.contains(item)) {
				foodlist.add(curruser);
			}
		}
		return foodlist;
	}

	/**
	 * This is a helper method to return the users by their food of either one of
	 * food items
	 * 
	 * @param fooditems[] contains of all of the food items
	 * @return
	 */
	public ArrayList<User> getfoodOrList(String[] fooditems) {
		HashSet<User> foodorlist = new HashSet<User>();
		for (int i = 0; i < this.userlist.size(); i++) {
			User newuser = this.userlist.get(i);
			for (int j = 0; j < newuser.foods.length; j++) {
				for (int k = 0; k < fooditems.length; k++) {
					if (newuser.foods[j] == fooditems[k]) {
						foodorlist.add(newuser);
					}
				}
			}
		}
		return new ArrayList<User>(foodorlist);
	}

	/**
	 * This is a helper method to return the users by their foods
	 * 
	 * @param fooditems food items array contains all the food items
	 * @return
	 */
	public ArrayList<User> getFoodAndList(String[] foodlist) {
		HashSet<User> foodAndList = new HashSet<User>();
		for (int i = 0; i < this.userlist.size(); i++) { // iterating through each user in user list

			User newuser = this.userlist.get(i);
			List<String> newuserfoods = Arrays.asList(newuser.foods);
			boolean areAllFound = true;
			for (String hobbyToSearch : foodlist) {
				if (!newuserfoods.contains(hobbyToSearch)) {
					areAllFound = false;
				}
			}
			if (areAllFound) {
				foodAndList.add(newuser);
			}
		}

		return new ArrayList<User>(foodAndList);
	}
/**
 * This is a helper method to get the user by their location entered
 * @param locationToSearch  name of the location for the user
 * @return
 */
	public ArrayList<User> getUserByLocation(String locationToSearch) {
		ArrayList<User> locationlist=new ArrayList<User>();
		for(User currentuser: this.userlist) {
			if(currentuser.location==locationToSearch) {
				locationlist.add(currentuser);
			}
		}
		return locationlist;
	}

}
