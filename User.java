//import java.util.ArrayList;

/**
 * this a user class to implement the user details
 * 
 * @author ayellapu1
 *
 */

public class User {
	// instance variables of user class
	public String	 name;
	public int 		 age;
	public String 	 designation;
	public float 	 weight;
	public String    food;
	public String[]  foods;
	public String 	 hobby;
	public String[]  hobbies;
	Gender			 gender_type;
	MaritalStatus 	 marital_status;
	Qualification[]  qualifications;
	public String 	 location;
	
	 
	/**
	 * constructor for creating new user
	 * 
	 * @param name        of the user
	 * @param age         age of the user
	 * @param destination destination of the user
	 * @param weight      weight of the user
	 * @param food        primary preference food for the user
	 * @param food[]      array of food items
	 * @param hobby       hobby of the user
	 * @param marital_status marraige status of the user
	 * @param location 
	 * @param gender_type2  gender of the user
	 * @param hobbies[]   array of hobbies for the user
	 * @param Q
	 * 
	 */
	public User(String name, int age, String designation, float weight, String food, String[] foods,
			String hobby, String[] hobbies, Gender gender_type, MaritalStatus marital_status,
			Qualification[] qualifications, String location) {
		this.name		 		= 		name;
		this.age 		 		= 		age;
		this.designation 		= 		designation;
		this.weight 	 		=		weight;
		this.food 		 		= 		food;
		this.foods 		 		= 		foods;
		this.hobby 		 		= 		hobby;
		this.hobbies 	 		= 		hobbies;
		this.gender_type 		=		gender_type;
		this.marital_status		=		marital_status;
		this.qualifications		=		qualifications;
		this.location           =		location;
	}
	/**
	 * helper method to return the user details in the human readable form
	 * 
	 * @return returnTome return the user details
	 */
	public String toString() {
		String returntome = " ";
		returntome += "		Display all user details  \n";
		returntome += "		 ---------------------------\n";
		returntome += "name		   :" + this.name + "\n";
		returntome += "age		   :" + this.age + "\n";
		returntome += "designation :" + this.designation + "\n";
		returntome += "weight	   :" + this.weight + "\n";
		returntome += "food		   :" + this.food + "\n";
		// loop to traverse the food array to append all food items under ones string
		String foodString = "[";
		for (int j = 0; j < this.foods.length; j++) {
			foodString += "\"" + this.foods[j] + "\",";
		}
		foodString += "]";
		returntome += "foods are    :" + foodString + "\n";
		returntome += "hobby		:" + this.hobby + "\n";
		System.out.println("\n");
		// loop to traverse the hobbies array to append all hobbies under one string
		String hobbiesString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbiesString += "\"" + this.hobbies[i] + "\",";
		}
		hobbiesString += "]";
		returntome += "hobbies are  :" + hobbiesString + "\n";
		returntome += "Gender 		:" + this.gender_type + "\n";
		returntome += "MaritalStatus:" + this.marital_status + "\n";
		// loop to traverse the qualifications  array to append all qualifications under one string
		String qualString = "[";
		for(int i=0;i< this.qualifications.length;i++) {
			qualString += "\""+ this.qualifications[i] + "\",";
		}
		qualString+="]";
		returntome += "qualifications are:  "+ qualString +"\n";
		return returntome;
	}

}
 enum Gender {
	MALE,
	FEMALE,
	PREFERNOTTOSAY;
}
 enum MaritalStatus {
		MARRIED,
		UNMARRIED,
		DIVORCED,
		NOTINTERETEDINMARRIAGE;

	}
