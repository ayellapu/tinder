
//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Scanner;

/**
 * this is a class to implement to test the social network application
 * 
 * @author ayellapu1
 *
 */

public class UserInterface {
	// main method

	public static void main(String[] args) {

		System.out.println("********* WELCOME TO TINDER APP ***********" + "\n");
		System.out.println("1. MAIN MENU :");
		System.out.println("2.EXIT" + "\n");
		System.out.println("ENTER YOUR CHOICE");

		Scanner sc = new Scanner(System.in);
		int anykey = sc.nextInt();
		switch (anykey) {
			case 1:
				
				UserCollection userColctn = new UserCollection();
				UserInterface ui = new UserInterface();
				/**
				 * Create objects to store the different
				 */
				Qualification q1 = new Qualification("MCA", "IT", "andhra university", 984, 2018, 3);
				Qualification q2 = new Qualification("btech", "cse", "acharya nagarjuna university", 876, 2018, 4);
				Qualification q3 = new Qualification("inter", "maths", "narayana junior college ", 990, 2020, 2);
				Qualification q4 = new Qualification("diploma", "civil", "gayatri college ", 865, 2020, 3);
				Qualification q5 = new Qualification("ssc", "science", "siddarth high school", 924, 2015, 2);
				/*
				 * create user collections object to call the new user method to create the new
				 * user and print all the users stored in the array list
				 */
	
				// method to create the new user
				userColctn.createNewUser("anil", 23, "software engineer", 76.5f, "biryani",
						new String[] { "biryani", "curd", "paneer" }, "cricket",
						new String[] { "dancing", "cricket", "watching", }, Gender.MALE, MaritalStatus.UNMARRIED,new Qualification[] {q1},"vizag");
				userColctn.createNewUser("kumar", 24, "IT manager", 55.6f, "panneer",
						new String[] { "potato fry", "panneer", "juice" }, "reading",
						new String[] { "games", "reading", "running" }, Gender.MALE, MaritalStatus.MARRIED,new Qualification[] {q2} ,"tuni");
				userColctn.createNewUser("sai", 24, "electrical engineer", 46.6f, "juice",
						new String[] { "mangoes", "juice", "vegetables" }, "vlogs",
						new String[] { "dancing", "vlogs", "carroms" }, Gender.FEMALE,
						MaritalStatus.NOTINTERETEDINMARRIAGE,new Qualification[] {q3},"vizag");
				userColctn.createNewUser("mani", 25, "software mainteanance", 37.8f, "biryani",
						new String[] { "biryani", "appadam", "juice" }, "cricket",
						new String[] { "chatting", "cricket", "running" }, Gender.PREFERNOTTOSAY, MaritalStatus.DIVORCED,new Qualification[] {q1,q4},"telangana");
				userColctn.createNewUser("bro", 24, "web desigining", 44.6f, "curd",
						new String[] { "curd", "mangoes", "vegetables" }, "comedy skits",
						new String[] { "comedy skits", "cricket", "trevelling" }, Gender.MALE, MaritalStatus.MARRIED,new Qualification[] {q5},"tirupati");
				userColctn.createNewUser("Jayanth", 23, "Developer", 56.8f, "fish",
						new String[] { "fish", "vegetables", "mutton" }, "Dancing",
						new String[] { "Guitarist", "vlogs", "Dancing" }, Gender.FEMALE, MaritalStatus.MARRIED,new Qualification[] {q2,q1},"rajamundry");
	
				
				ui.userInterface(userColctn);
				break;
			default:
				System.out.println("Thanks for visiting tinder app");
				break;
		}
		sc.close();
		

	}

	public void userInterface(UserCollection usrClctnObj) {
		System.out.println("1.TO VIEW THE ALL USERS");
		System.out.println("2.DISPLAY USERS MATCHED BY GIVEN NAME");
		System.out.println("3.DISPLAY THE USERS WHO HAD THE COMMON HOBBIES");
		System.out.println("4.DISPLAY THE USERS BY ENTERED HOBBY");
		System.out.println("5.DISPLAY THE USERS BY OR HOBBIES");
		System.out.println("6.DISPLAY THE USERS BY AND HOBBIES");
		System.out.println("7.DISPLAY THE USERS BY ENTERED DESIGNATION");
		System.out.println("8.DISPLAY THE USERS BY MINIMUM WEIGHTS ");
		System.out.println("9.DISPLAY THE USERS BY MAXIMUM WEIGHTS");
		System.out.println("10.DISPLAY THE USERS BY WEIGHTS IN BETWEN");
		System.out.println("11.DISPLAY THE USERS BY ENTERED FOOD");
		System.out.println("12.DISPLAY THE USERS BY OR FOOD");
		System.out.println("13.DSIPLAY THE USERS BY AND FOOD");
		System.out.println("14.DISPLAY THE USERS BY LOCATION");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter your choice");
		int choice = sc.nextInt();
		switch (choice) {
		case 1:
			// printing the users stored in the array list
			System.out.println("*******ALL THE USERS DETAILS ARE***********"); 
			usrClctnObj.returnAllUsers();
			
			break;
		case 2:
			// call a get match method to return the user matched by given name
			System.out.println("display user matched by given name:");
			LinkedHashSet<User> Results = usrClctnObj.getMatch("mani"); 
			for (User name : Results) {
				System.out.println(name);
			}
			break;
		case 3:
			// call a name match method to return the users who had the common hobbies 
			ArrayList<User> listOfSelectedset = usrClctnObj.getNameMatch("anil"); 
			for (User u : listOfSelectedset) {
				System.out.println(u);
			}
			break;
		case 4:
			// call a method to return the users by the hobby entered
			System.out.println("display all user by hobby");
			ArrayList<User> hobbyList = usrClctnObj.getHobbyList("cricket"); 
			for (User v : hobbyList) {
				System.out.println(v);
			}
			break;
		case 5:
			//call a method to return the users who had the one of the food entered
			System.out.println("dispaly all user by either or hobbies");
			ArrayList<User> hobbyORlist = usrClctnObj.getHobbyOrList(new String[] { "cricket", "carroms", "dancing" });				
			for (User w : hobbyORlist) {
				System.out.println(w);
			}
			break;
		case 6:
			// call a method to return the users who had the both hobbies entered
			System.out.println("display all users having both hobbies");
			ArrayList<User> hobbyANDlist = usrClctnObj.getHobbyAndList(new String[] { "cricket", "running" });																																														// hobbies
			for (User z : hobbyANDlist) {
				System.out.println(z);
			}
			break;
		case 7:
			// call a method to return the users by their designation
			System.out.println("display all the users by designation ");
			ArrayList<User> designationlist = usrClctnObj.getDesignation("software engineer");
			for (User a : designationlist) {
				System.out.println(a);
			}
			break;
		case 8:
			// call a method to return the users who had the lesser weight than the entered weight
			System.out.println("display all the users by minimum weight");
			ArrayList<User> minweightlist = usrClctnObj.getMinWeight(60.0f);
			for (User b : minweightlist) {
				System.out.println(b);
			}
			break;
		case 9:
			//call a method to return the users who the higher weight than the entered weight 
			System.out.println("display all the users by maximum weight ");
			ArrayList<User> maxweightlist = usrClctnObj.getMaxWeight(55.0f);
			for (User c : maxweightlist) {
				System.out.println(c);
			}
			break;
		case 10:
			// call a method to return the users who had weights in between the range entered
			System.out.println("display the users by weights inbetwen");
			ArrayList<User> betweight = usrClctnObj.getBetWeight(20.5f, 50.0f);
			for (User d : betweight) {
				System.out.println(d);
			}
			break;
		case 11:
			// call a method to return the users by their food
			System.out.println("display the users by their foods");
			ArrayList<User> foodlist = usrClctnObj.getFoodMatch("biryani");
			for (User e : foodlist) {
				System.out.println(e);
			}
			break;
		case 12:
			// call a method to return the users who had the one of food entered
			System.out.println("dispaly all user by either or foods");
			ArrayList<User> foodORlist = usrClctnObj.getfoodOrList(new String[] { "fish", "biryani" });
			for (User f : foodORlist) {
				System.out.println(f);
			}
			break;
		case 13:
			// call a method to return the users who had the both the foods
			System.out.println("display all user by having both the foods");
			ArrayList<User> foodAndlist = usrClctnObj.getFoodAndList(new String[] { "biryani", "curd" });
			for (User g : foodAndlist) {
				System.out.println(g);
			}
			break;
		case 14 :
			// call a method to return the users by their location
			System.out.println("display the users by their location");
			ArrayList<User> locationlist=usrClctnObj.getUserByLocation("vizag");
			for(User h:locationlist) {
				System.out.println(h);
			}
			break;
		
		}
sc.close();
	}

}