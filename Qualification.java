/**
 * This is a class for implementing the qualification details
 * @author ayellapu1
 *
 */
public class Qualification {
	public String 	course_name;
	public String 	course_branch;
	public String 	college;
	public int  	marks;
	public int  	year_of_passedout;
	public int 		duration;
	
	/**
	 * constructor for user qualification details
	 * @param 	course_name 			course name of the user
	 * @param 	course_branch 			course branch for the user
	 * @param 	college  				name of the college in which user studied
	 * @param 	marks    		    	marks gained by the user
	 * @param	year_of_passedout 		year in which the user passed out from the college
	 * @param 	duration 				duration of the course
	 */
	public Qualification(String course_name, String course_branch, String college, int marks, int year_of_passedout,int duration ) {
		this.course_name 		=		course_name;
		this.course_branch		=			course_branch;
		this.college			=			college;
		this.marks				=			marks;
		this.year_of_passedout	=			year_of_passedout;
		this.duration			=			duration;
		
	}
	/**
	 * This is a helper method to return qualification details in human readable form
	 */
	public String toString() {
		String qualReturn ="";
		 qualReturn +=  this.course_name +","+this.course_branch+","+this.college+","+this.marks+","+this.year_of_passedout+","
				 +this.duration;
		 return qualReturn;
	}

}
